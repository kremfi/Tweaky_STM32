/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    App/custom_app.c
  * @author  MCD Application Team
  * @brief   Custom Example Application (Server)
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_common.h"
#include "dbg_trace.h"
#include "ble.h"
#include "custom_app.h"
#include "custom_stm.h"
#include "stm32_seq.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
extern UART_HandleTypeDef huart1;
extern stmdev_ctx_t lsm6dsoDriver;
extern ADC_HandleTypeDef hadc1;

#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
  /* Dashboard */
  uint8_t               Vltg_Notification_Status;
  uint8_t               Vltg_Indication_Status;
  uint8_t               Tmp_Notification_Status;
  uint8_t               Tmp_Indication_Status;
  uint8_t               Imu_Notification_Status;
  uint8_t               Imu_Indication_Status;
  uint8_t               Dutyservo_Notification_Status;
  uint8_t               Dutyservo_Indication_Status;
  uint8_t               Dutymotor_Notification_Status;
  uint8_t               Dutymotor_Indication_Status;
  /* USER CODE BEGIN CUSTOM_APP_Context_t */

  /* USER CODE END CUSTOM_APP_Context_t */

  uint16_t              ConnectionHandle;
} Custom_App_Context_t;

/* USER CODE BEGIN PTD */
typedef struct
{
  uint8_t               Vltg_value[4];
  uint8_t          		Tmp_value[4];
  uint8_t               IMU_value[6];
  uint8_t               Lights;
  uint8_t				PWM;
  uint8_t	  	  	  	Update_timer_ID;
  uint8_t	  	  	  	Vltg_timer_ID;
  uint8_t	  	  	  	Tmp_timer_ID;
  uint8_t	  	  	  	IMU_timer_ID;
} Dashboard;
/* USER CODE END PTD */

/* Private defines ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
typedef union {
int16_t i16bit;
uint8_t u8bit[2];
} axis1bit16_t;
typedef union {
int32_t i32bit;
uint8_t u8bit[4];
} axis1bit32_t;
typedef union {
int16_t i16bit[3];
uint8_t u8bit[6];
} axis3bit16_t;
/* USER CODE END PD */

/* Private macros -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
uint8_t reg_LSM6DSO;
static axis3bit16_t data_raw_acceleration_LSM6DSO; static float acceleration_mg_LSM6DSO[3];
static axis3bit16_t data_raw_angular_rate_LSM6DSO; static float angular_rate_mdps_LSM6DSO[3];
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/**
 * START of Section BLE_APP_CONTEXT
 */

static Custom_App_Context_t Custom_App_Context;

/**
 * END of Section BLE_APP_CONTEXT
 */

uint8_t UpdateCharData[247];
uint8_t NotifyCharData[247];

/* USER CODE BEGIN PV */

void LEDToggle(void);
void LEDToggleUpdate(void);

lps22hh_reg_t reg_LPS22HH;
static axis1bit32_t data_raw_pressure_LPS22HH;
static float pressure_hPa_LPS22HH;
static axis1bit16_t data_raw_temperature_LPS22HH;
static float temperature_degC_LPS22HH;

uint8_t timer_id = 0;
uint8_t TimerId = 12;
uint8_t TimerId_2 = 13;
Dashboard data;
uint8_t toggler = 0;
uint8_t init = 1;
uint8_t rx_message[10];
uint8_t i = 0;
uint8_t buffer[64];
uint8_t SteeringDuty;
uint8_t ThrottleDuty;
uint8_t transmit= 0;
uint16_t ADC_measurement[6];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* Dashboard */
static void Custom_Vltg_Update_Char(void);
static void Custom_Vltg_Send_Notification(void);
static void Custom_Vltg_Send_Indication(void);
static void Custom_Tmp_Update_Char(void);
static void Custom_Tmp_Send_Notification(void);
static void Custom_Tmp_Send_Indication(void);
static void Custom_Imu_Update_Char(void);
static void Custom_Imu_Send_Notification(void);
static void Custom_Imu_Send_Indication(void);
static void Custom_Dutyservo_Update_Char(void);
static void Custom_Dutyservo_Send_Notification(void);
static void Custom_Dutyservo_Send_Indication(void);
static void Custom_Dutymotor_Update_Char(void);
static void Custom_Dutymotor_Send_Notification(void);
static void Custom_Dutymotor_Send_Indication(void);

/* USER CODE BEGIN PFP */

void Dashboard_Measurement (void);
void Dashboard_Measurement_Update (void);
void Dashboard_Measurement_Vltg (void);
void Dashboard_Measurement_Tmp (void);
void Dashboard_Measurement_Speed (void);

void Dashboard_Measurement_Vltg (void)
{
	printf("ADC_Voltage1: %d --- ADC_Voltage2: %d\n",ADC_measurement[2],ADC_measurement[3]);
	data.Vltg_value[0] = ADC_measurement[2];
	data.Vltg_value[1] = ADC_measurement[2] >> 8;
	data.Vltg_value[2] = ADC_measurement[3];
	data.Vltg_value[3] = ADC_measurement[3] >> 8;

}

void Dashboard_Measurement_Tmp (void)
{
	printf("ADC_Temperature1: %d --- ADC_Temperature2: %d\n",ADC_measurement[4],ADC_measurement[5]);
	data.Tmp_value[0] = ADC_measurement[4];
	data.Tmp_value[1] = ADC_measurement[4] >> 8;
	data.Tmp_value[2] = ADC_measurement[5];
	data.Tmp_value[3] = ADC_measurement[5] >> 8;

}


void Dashboard_Measurement_Speed (void)
{
		lsm6dso_xl_flag_data_ready_get(&lsm6dsoDriver, &reg_LSM6DSO);
		if (reg_LSM6DSO) {
		//printf("Accelo okay!");
		memset(data_raw_acceleration_LSM6DSO.i16bit, 0x00, 3 * sizeof(int16_t));
		lsm6dso_acceleration_raw_get(&lsm6dsoDriver, data_raw_acceleration_LSM6DSO.i16bit);

		for(int i = 0;i < 6;i++)
		{
			data.IMU_value[i] = lsm6dso_from_fs8_to_mg(data_raw_acceleration_LSM6DSO.u8bit[i]);

		}

		}



		//uint8_t *data = "Hello World from USB CDC\n";
		//uint8_t res = CDC_Transmit_FS(data, strlen((const char *)data));

		//printf("First Value. %d, Second Value: %d\n",Raw[0],Raw[1]);
		//printf("Volt First. %d, Second Volt: %d\n",Raw[2],Raw[3]);
		//printf("First Temp. %d, Second Temp: %d\n",Raw[4],Raw[5]);
}


void Dashboard_Measurement (void)
{
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);


	Custom_STM_App_Update_Char(CUSTOM_STM_VLTG, (uint8_t *)&data.Vltg_value);
	Custom_STM_App_Update_Char(CUSTOM_STM_TMP, (uint8_t *)&data.Tmp_value);
	Custom_STM_App_Update_Char(CUSTOM_STM_IMU, (uint8_t *)&data.IMU_value);
}

void Dashboard_Measurement_Update (void)
{
	UTIL_SEQ_SetTask( 1<<CFG_TASK_VALUE_MEASURE_ID,CFG_SCH_PRIO_0);
}




/***LED TOGGLING FOR BLE ADVERTISING**/
void LEDToggle(void)
{
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15);
}

void LEDToggleUpdate(void)
{
	UTIL_SEQ_SetTask(1 << CFG_LED_TOGGLE, CFG_SCH_PRIO_0);
}


/*void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	LEDToggleUpdate();
}*/

/* USER CODE END PFP */

/* Functions Definition ------------------------------------------------------*/
void Custom_STM_App_Notification(Custom_STM_App_Notification_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_1 */

  /* USER CODE END CUSTOM_STM_App_Notification_1 */
  switch (pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

    /* USER CODE END CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

    /* Dashboard */
    case CUSTOM_STM_VLTG_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_READ_EVT */

      /* USER CODE END CUSTOM_STM_VLTG_READ_EVT */
      break;

    case CUSTOM_STM_VLTG_WRITE_NO_RESP_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_WRITE_NO_RESP_EVT */
    	if(pNotification->DataTransfered.pPayload[0] == 0x00)
    	    {
    	    	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0,GPIO_PIN_RESET);
    			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1,GPIO_PIN_SET);
    	    }
    	else if(pNotification->DataTransfered.pPayload[0] == 0x01)
    	    {
    	    	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0,GPIO_PIN_SET);
    			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1,GPIO_PIN_SET);
    	    }
      /* USER CODE END CUSTOM_STM_VLTG_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_VLTG_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_NOTIFY_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_VLTG_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_VLTG_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_NOTIFY_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_VLTG_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_VLTG_INDICATE_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_INDICATE_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_VLTG_INDICATE_ENABLED_EVT */
      break;

    case CUSTOM_STM_VLTG_INDICATE_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_VLTG_INDICATE_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_VLTG_INDICATE_DISABLED_EVT */
      break;

    case CUSTOM_STM_TMP_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_READ_EVT */

      /* USER CODE END CUSTOM_STM_TMP_READ_EVT */
      break;

    case CUSTOM_STM_TMP_WRITE_NO_RESP_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_WRITE_NO_RESP_EVT */

    	/*
    	if(pNotification->DataTransfered.pPayload[0] == 0x00)
    	    {
    	     	 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1,GPIO_PIN_RESET);
    	    }
    	else if(pNotification->DataTransfered.pPayload[0] == 0x01)
    	    {
    	         HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1,GPIO_PIN_SET);
    	    }
		*/
      /* USER CODE END CUSTOM_STM_TMP_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_TMP_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_NOTIFY_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_TMP_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_TMP_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_NOTIFY_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_TMP_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_TMP_INDICATE_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_INDICATE_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_TMP_INDICATE_ENABLED_EVT */
      break;

    case CUSTOM_STM_TMP_INDICATE_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMP_INDICATE_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_TMP_INDICATE_DISABLED_EVT */
      break;

    case CUSTOM_STM_IMU_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_READ_EVT */

      /* USER CODE END CUSTOM_STM_IMU_READ_EVT */
      break;

    case CUSTOM_STM_IMU_WRITE_NO_RESP_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_WRITE_NO_RESP_EVT */

      /* USER CODE END CUSTOM_STM_IMU_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_IMU_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_NOTIFY_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_IMU_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_IMU_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_NOTIFY_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_IMU_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_IMU_INDICATE_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_INDICATE_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_IMU_INDICATE_ENABLED_EVT */
      break;

    case CUSTOM_STM_IMU_INDICATE_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_IMU_INDICATE_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_IMU_INDICATE_DISABLED_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_READ_EVT */

      /* USER CODE END CUSTOM_STM_DUTYSERVO_READ_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_WRITE_NO_RESP_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_WRITE_NO_RESP_EVT */
    	TIM2->CCR1 = pNotification->DataTransfered.pPayload[0];
      /* USER CODE END CUSTOM_STM_DUTYSERVO_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_NOTIFY_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYSERVO_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_NOTIFY_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYSERVO_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_INDICATE_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_INDICATE_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYSERVO_INDICATE_ENABLED_EVT */
      break;

    case CUSTOM_STM_DUTYSERVO_INDICATE_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYSERVO_INDICATE_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYSERVO_INDICATE_DISABLED_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_READ_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_READ_EVT */

      /* USER CODE END CUSTOM_STM_DUTYMOTOR_READ_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_WRITE_NO_RESP_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_WRITE_NO_RESP_EVT */
    	TIM1->CCR3 = pNotification->DataTransfered.pPayload[0];
      /* USER CODE END CUSTOM_STM_DUTYMOTOR_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_NOTIFY_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYMOTOR_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_NOTIFY_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYMOTOR_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_INDICATE_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_INDICATE_ENABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYMOTOR_INDICATE_ENABLED_EVT */
      break;

    case CUSTOM_STM_DUTYMOTOR_INDICATE_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_DUTYMOTOR_INDICATE_DISABLED_EVT */

      /* USER CODE END CUSTOM_STM_DUTYMOTOR_INDICATE_DISABLED_EVT */
      break;

    default:
      /* USER CODE BEGIN CUSTOM_STM_App_Notification_default */

      /* USER CODE END CUSTOM_STM_App_Notification_default */
      break;
  }
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_2 */

  /* USER CODE END CUSTOM_STM_App_Notification_2 */
  return;
}

void Custom_APP_Notification(Custom_App_ConnHandle_Not_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_APP_Notification_1 */

  /* USER CODE END CUSTOM_APP_Notification_1 */

  switch (pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_APP_Notification_Custom_Evt_Opcode */

    /* USER CODE END P2PS_CUSTOM_Notification_Custom_Evt_Opcode */
    case CUSTOM_CONN_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_CONN_HANDLE_EVT */

      /* USER CODE END CUSTOM_CONN_HANDLE_EVT */
      break;

    case CUSTOM_DISCON_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_DISCON_HANDLE_EVT */

      /* USER CODE END CUSTOM_DISCON_HANDLE_EVT */
      break;

    default:
      /* USER CODE BEGIN CUSTOM_APP_Notification_default */

      /* USER CODE END CUSTOM_APP_Notification_default */
      break;
  }

  /* USER CODE BEGIN CUSTOM_APP_Notification_2 */

  /* USER CODE END CUSTOM_APP_Notification_2 */

  return;
}

void Custom_APP_Init(void)
{
  /* USER CODE BEGIN CUSTOM_APP_Init */
	//UTIL_SEQ_RegTask(1<< CFG_MEASURE_VOLTAGE, UTIL_SEQ_RFU, MeasureVoltage);
	//UTIL_SEQ_RegTask(1<< CFG_MEASURE_TEMPERATURE, UTIL_SEQ_RFU, MeasureTemperature);
	//UTIL_SEQ_RegTask(1<< CFG_LED_TOGGLE, UTIL_SEQ_RFU, LEDToggle);

	//UTIL_SEQ_SetTask(1 << CFG_MEASURE_VOLTAGE, CFG_SCH_PRIO_0);
	//UTIL_SEQ_SetTask(1 << CFG_MEASURE_TEMPERATURE, CFG_SCH_PRIO_0);

	/**DASHBOARD DATA INIT**/

	//HAL_UART_Receive_DMA (&huart1, rx_message, 1);

	/*for(int i = 0;i < 6;i++)
		data.IMU_value[i] = 0;*/
	//memset(data.IMU_value, 0,6*sizeof(uint8_t));
	//memset(data.Vltg_value, 0, 4*sizeof(uint8_t));
	//memset(data.Tmp_value, 0, 4*sizeof(uint8_t));

	data.Lights = 0;
	data.PWM = 0;
	data.Update_timer_ID = 0;
	data.Vltg_timer_ID = 1;
	data.Tmp_timer_ID = 2;
	data.IMU_timer_ID = 3;

	HAL_ADCEx_Calibration_Start(&hadc1,ADC_SINGLE_ENDED);
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)ADC_measurement, 6);

	UTIL_SEQ_RegTask( 1<< CFG_TASK_VALUE_MEASURE_ID, UTIL_SEQ_RFU, Dashboard_Measurement);

	HW_TS_Create(CFG_TIM_PROC_ID_ISR, &data.Update_timer_ID, hw_ts_Repeated, Dashboard_Measurement_Update);
	HW_TS_Start(data.Update_timer_ID, (1000000/CFG_TS_TICK_VAL)/5);

	HW_TS_Create(CFG_TIM_VLTG, &data.Vltg_timer_ID, hw_ts_Repeated, Dashboard_Measurement_Vltg);
	HW_TS_Start(data.Vltg_timer_ID, (1000000/CFG_TS_TICK_VAL));

	HW_TS_Create(CFG_TIM_TMP, &data.Tmp_timer_ID, hw_ts_Repeated, Dashboard_Measurement_Tmp);
	HW_TS_Start(data.Tmp_timer_ID, (1000000/CFG_TS_TICK_VAL));

	HW_TS_Create(CFG_TIM_IMU, &data.IMU_timer_ID, hw_ts_Repeated, Dashboard_Measurement_Speed);
	HW_TS_Start(data.IMU_timer_ID, (1000000/CFG_TS_TICK_VAL)/5);

  /* USER CODE END CUSTOM_APP_Init */
  return;
}

/* USER CODE BEGIN FD */
/*void HAL_GPIO_EXTI_Callback( uint16_t GPIO_Pin )
{
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);


}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);

	HAL_UART_Receive_DMA(&huart1, buffer, 1);

	if(transmit == 0)
	{
		//uint8_t rx[5] = "HELLO";
		rx_message[i] = buffer[0];
		i++;
		HAL_UART_Transmit_DMA(&huart1, rx_message, i);
		transmit = 1;
	}



}*/


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
	transmit=0;


}

int _write(int file, char *ptr, int len) {
    CDC_Transmit_FS((uint8_t*) ptr, len); return len;
}





/* USER CODE END FD */

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/

/* Dashboard */
void Custom_Vltg_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Vltg_UC_1*/

  /* USER CODE END Vltg_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_VLTG, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Vltg_UC_Last*/

  /* USER CODE END Vltg_UC_Last*/
  return;
}

void Custom_Vltg_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Vltg_NS_1*/

  /* USER CODE END Vltg_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_VLTG, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Vltg_NS_Last*/

  /* USER CODE END Vltg_NS_Last*/

  return;
}

void Custom_Vltg_Send_Indication(void) /* Property Indication */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Vltg_IS_1*/

  /* USER CODE END Vltg_IS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_VLTG, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Vltg_IS_Last*/

  /* USER CODE END Vltg_IS_Last*/

  return;
}

void Custom_Tmp_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Tmp_UC_1*/

  /* USER CODE END Tmp_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_TMP, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Tmp_UC_Last*/

  /* USER CODE END Tmp_UC_Last*/
  return;
}

void Custom_Tmp_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Tmp_NS_1*/

  /* USER CODE END Tmp_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_TMP, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Tmp_NS_Last*/

  /* USER CODE END Tmp_NS_Last*/

  return;
}

void Custom_Tmp_Send_Indication(void) /* Property Indication */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Tmp_IS_1*/

  /* USER CODE END Tmp_IS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_TMP, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Tmp_IS_Last*/

  /* USER CODE END Tmp_IS_Last*/

  return;
}

void Custom_Imu_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Imu_UC_1*/

  /* USER CODE END Imu_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_IMU, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Imu_UC_Last*/

  /* USER CODE END Imu_UC_Last*/
  return;
}

void Custom_Imu_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Imu_NS_1*/

  /* USER CODE END Imu_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_IMU, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Imu_NS_Last*/

  /* USER CODE END Imu_NS_Last*/

  return;
}

void Custom_Imu_Send_Indication(void) /* Property Indication */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Imu_IS_1*/

  /* USER CODE END Imu_IS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_IMU, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Imu_IS_Last*/

  /* USER CODE END Imu_IS_Last*/

  return;
}

void Custom_Dutyservo_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutyservo_UC_1*/

  /* USER CODE END Dutyservo_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYSERVO, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Dutyservo_UC_Last*/

  /* USER CODE END Dutyservo_UC_Last*/
  return;
}

void Custom_Dutyservo_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutyservo_NS_1*/

  /* USER CODE END Dutyservo_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYSERVO, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Dutyservo_NS_Last*/

  /* USER CODE END Dutyservo_NS_Last*/

  return;
}

void Custom_Dutyservo_Send_Indication(void) /* Property Indication */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutyservo_IS_1*/

  /* USER CODE END Dutyservo_IS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYSERVO, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Dutyservo_IS_Last*/

  /* USER CODE END Dutyservo_IS_Last*/

  return;
}

void Custom_Dutymotor_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutymotor_UC_1*/

  /* USER CODE END Dutymotor_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYMOTOR, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Dutymotor_UC_Last*/

  /* USER CODE END Dutymotor_UC_Last*/
  return;
}

void Custom_Dutymotor_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutymotor_NS_1*/

  /* USER CODE END Dutymotor_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYMOTOR, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Dutymotor_NS_Last*/

  /* USER CODE END Dutymotor_NS_Last*/

  return;
}

void Custom_Dutymotor_Send_Indication(void) /* Property Indication */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Dutymotor_IS_1*/

  /* USER CODE END Dutymotor_IS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_DUTYMOTOR, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Dutymotor_IS_Last*/

  /* USER CODE END Dutymotor_IS_Last*/

  return;
}

/* USER CODE BEGIN FD_LOCAL_FUNCTIONS*/

/* USER CODE END FD_LOCAL_FUNCTIONS*/
